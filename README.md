# **Inventaario - työkalu** 
## **TODO:**
+ Tavarahaku
+ Tavara-näkymään kategoriat joihin tuote kuuluu
+ Tavara-näkymään laskettu yhteen tavaran lukumäärä
+ Tavaralle mahdollisuus lisätä kuvaus
+ Kategoria haku
+ Asetukset osio
+ Dokumentaatio
+ CSS-tyylien tekeminen
+ Visuaalisen ilmeen kohentaminen

## **DONE:**
+ Käyttäjän luonti
+ Kirjautuminen
+ Kategorien luonti
+ Kategorien poistaminen
+ Inventaario rivien lisääminen
+ Inventaario rivien poistaminen
+ Tavaran lisääminen
+ Tavaran muokkaaminen
+ Tavaran poistaminen