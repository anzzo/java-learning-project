
package Controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import Exceptions.CategoryRowsException;
import Exceptions.InventoryException;
import SQLite.Query;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import Main.Main;
import Models.Category;
import Models.CategoryRows;
import Models.Model;
import Models.Stuff;
import Models.User;

public class CategoryController extends Controller{

    public ScreenController scc;
    public int category_id; 

    private EventHandler<ActionEvent> button_handler = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            try{
                Button button      = (Button) event.getSource();
                String buttonId    = button.getId();
                String button_name = buttonId.split("_")[0];
                int id = Integer.parseInt(buttonId.split("_")[1]);
                Scene scene = button.getScene();
                Label label = (Label) scene.lookup("#label_" + id);
                HBox row = (HBox) scene.lookup("#categoryrow_" + id);
                int label_now = Integer.parseInt(label.getText());
                switch (button_name) {
                    case "add": 
                    CategoryRows.addOne(id);
                    label.setText((label_now + 1) + "");
                    break;
                    case "decrease": 
                    CategoryRows.decreaseOne(id); 
                    label.setText((label_now - 1) + "");
                    break;
                    case "remove":
                    new CategoryRows().removeRow(id);
                    row_container.getChildren().remove(row);
                    break;
                    case "hyperlink":
                    scc.addToVariableList("stuff_id", id);
                    scc.activate("stuff");
                    break;
                    default: break;
                }                                
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }catch(InventoryException e){
                System.out.println(e.getMessage());
            }
        }
    };

    private EventHandler<ActionEvent> hyperlink_handler = new EventHandler<ActionEvent>() {
        @Override
        public void handle(ActionEvent event) {
            try {
                Hyperlink hyperlink = (Hyperlink) event.getSource();
                String hyperlinkId = hyperlink.getId();
                String hyperlink_name = hyperlinkId.split("_")[0];
                int id = Integer.parseInt(hyperlinkId.split("_")[1]);
                switch (hyperlink_name) {
                    case "stufflink":
                        scc.addToVariableList("stuff_id", id);
                        scc.addToVariableList("show_back", true);
                        scc.activate("stuff");
                        break;
                    default:
                        break;
                }
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            } catch (InventoryException e) {
                System.out.println(e.getMessage());
            }
        }
    };

    @FXML
    private VBox row_container;

    @FXML
    private Text header_text;

    @FXML
    private Button add_row;

    @FXML
    private Button add_stuff;

    @FXML
    private Button remove_category;

    @FXML
    protected void createNewStuff(ActionEvent event) throws SQLException, InventoryException{
        TextInputDialog dialog = new TextInputDialog("Tavaran nimi tähän...");
        dialog.setTitle("Luo uusi tavara");
        dialog.setHeaderText("Luo uusi tavara kirjoittamalla sen nimi alle ja klikkaamalla \"OK\"");

        DialogPane pane = dialog.getDialogPane();
        pane.getStylesheets().add(getClass().getResource(Main.cssPath + Main.universal).toExternalForm());
        pane.getStyleClass().add("newStuffDialog");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Model stuff = new Stuff().create(result.get(), "TODO", new Stuff().getNextOrder()).save();
            Model categoryrow = new CategoryRows().create(this.category_id, stuff.modelValues.get("id"), 1, new CategoryRows().getNextOrder());
            if (categoryrow.save().exists()) {
                this.addRow(categoryrow.modelValues, stuff.modelValues);
            }
        }
    }

    @FXML
    protected void addCategoryRow(ActionEvent event) throws SQLException, InventoryException{
        ArrayList<HashMap<String,Object>> results = new Query().select("*").from(Stuff.TABLE).execute();
        List<String> choices = new ArrayList<>();
        for(HashMap<String, Object> row : results){
            choices.add((String) row.get("name"));
        }
        ChoiceDialog<String> dialog = new ChoiceDialog<String>(choices.get(0), choices);
        dialog.setHeaderText("Valitse tavara.");
        dialog.setContentText("Valitse tavara joka lisätään kategoriaan.");

        DialogPane pane = dialog.getDialogPane();
        pane.getStylesheets().add(getClass().getResource(Main.cssPath + Main.universal).toExternalForm());
        pane.getStyleClass().add("getStuffDialog");

        Optional<String> result = dialog.showAndWait();
        if(result.isPresent()){
            System.out.println(result.get());
            Stuff stuff = new Stuff(new Query().select("*").from(Stuff.TABLE).where("name", result.get()).execute().get(0));
            if(stuff.exists()){
                CategoryRows categoryrow = (CategoryRows) new CategoryRows().create(this.category_id, stuff.modelValues.get("id"), 1, new CategoryRows().getNextOrder());
                if(categoryrow.save().exists()){
                    this.addRow(categoryrow.modelValues, stuff.modelValues);
                }
            }
        }
    }

    public void onLoad(HashMap<String, Object> list) throws SQLException, InventoryException{
        ArrayList<HashMap<String, Object>> rows = new ArrayList<HashMap<String, Object>>();
        User user = scc.getUser();
        add_row.managedProperty().bind(add_row.visibleProperty());
        add_stuff.managedProperty().bind(add_stuff.visibleProperty());
        remove_category.managedProperty().bind(remove_category.visibleProperty());
        String text = null;
        if(list.get("show_all") == null){
            this.category_id = (int) list.get("category_id");
            rows = new Query().select("*")
                              .from(CategoryRows.TABLE)
                              .where("category_id", this.category_id)
                              .execute();
            text = (String) new Query().select("name").from("category").where("id", this.category_id).execute().get(0).get("name");
            add_row.setVisible(true);
            add_stuff.setVisible(true);
            remove_category.setVisible(true);
        }else{
            ArrayList<HashMap<String, Object>> categories = new Query().select("*")
                                                                       .from(Category.TABLE)
                                                                       .where("user_id", (int) user.modelValues.get("id"))
                                                                       .execute();
            for(HashMap<String, Object> cat_row : categories){
                ArrayList<HashMap<String, Object>> row_row = new Query().select("*")
                                                             .from(CategoryRows.TABLE)
                                                             .where("category_id", (int) cat_row.get("id"))
                                                             .execute();
                for(HashMap<String, Object> rowi : row_row){
                    rows.add(rowi);
                }
            }
            text = "Kaikki";
            add_row.setVisible(false);
            add_stuff.setVisible(false);
            remove_category.setVisible(false);
        }
        if(rows == null){
            throw new CategoryRowsException("Coludn't load rows for category view");
        }
            
        ArrayList<HashMap<String, Object>> stuffs = new Query().select("*")
                                                               .from(Stuff.TABLE)
                                                               .execute();
                                 
        header_text.setText(text);

        row_container.getChildren().clear();
        for(HashMap<String, Object> row : rows){
            for(HashMap<String, Object> stuff : stuffs){
                if((int) row.get("stuff_id") == (int) stuff.get("id")){
                    this.addRow(row, stuff);
                }
            }
        }
    }

    /**
     * Adds row to row_container
     * @param category_row {HashMap<String, Object>} - One category
     * @param stuff {HashMap<String, Object>} - One stuff
     */
    public void addRow(HashMap<String, Object> category_row, HashMap<String, Object> stuff){
        Double button_width = 50.0;

        Button add_button = new Button("+");
        add_button.setId("add_" + category_row.get("id"));
        add_button.prefWidth(button_width);
        add_button.setOnAction(button_handler);
        
        Button decrease_button = new Button("-");
        decrease_button.setId("decrease_" + category_row.get("id"));
        decrease_button.prefWidth(button_width);
        decrease_button.setOnAction(button_handler);

        Button remove_button = new Button("Poista");
        remove_button.setId("remove_" + category_row.get("id"));
        remove_button.prefWidth(button_width);
        remove_button.setOnAction(button_handler);

        Label amount = new Label("" + category_row.get("amount"));
        amount.setPadding(new Insets(0, 20, 0, 0));
        amount.setId("label_" + category_row.get("id"));
        
        HBox button_container = new HBox();
        button_container.getStyleClass().add("grey");
        button_container.setAlignment(Pos.CENTER_RIGHT);

        button_container.getChildren().add(amount);
        button_container.getChildren().add(add_button);
        button_container.getChildren().add(decrease_button);
        button_container.getChildren().add(remove_button);
        
        HBox text_container = new HBox();
        text_container.setId("categoryrow_" + category_row.get("id"));
        text_container.getStyleClass().add("grey");

        Hyperlink hyperlink = new Hyperlink("" + stuff.get("name"));
        hyperlink.setId("stufflink_" + stuff.get("id"));
        hyperlink.setOnAction(hyperlink_handler);
        hyperlink.setTextFill(Paint.valueOf("black"));
        
        text_container.getChildren().add(hyperlink);
        text_container.getChildren().add(button_container);
        HBox.setHgrow(button_container, Priority.ALWAYS);

        row_container.getChildren().add(text_container);
    }
    
    @FXML
    protected void goBack(ActionEvent event) throws SQLException, InventoryException{
        scc.activate("home");
    }

    @FXML
    protected void removeCategory(ActionEvent event) throws SQLException, InventoryException{
        ArrayList<HashMap<String, Object>> category_rows = new Query().select("*").from(CategoryRows.TABLE).where("category_id", category_id).execute();
        for(HashMap<String, Object> row : category_rows){
            new CategoryRows().removeRow((int) row.get("id"));
        }
        new Category().removeRow(category_id);
        scc.activate("home");
    }
}