
package Controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;

import Exceptions.InventoryException;
import Main.Main;
import Models.Category;
import Models.Stuff;
import Models.User;
import SQLite.Query;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class HomeController extends Controller{

    public User user;
    public ScreenController scc;

    private EventHandler<ActionEvent> button_handler = new EventHandler<ActionEvent>(){
        @Override
        public void handle(ActionEvent event){
            try{
                Button button        = (Button) event.getSource();
                String buttonId      = button.getId();
                String button_name   = buttonId.split("_")[0];
                int    id            = Integer.parseInt(buttonId.split("_")[1]);
                switch(button_name){
                    case "category":
                    scc.addToVariableList("category_id", id);
                    scc.activate("category");
                    break;
                    case "stuff":
                    scc.addToVariableList("stuff_id", id);
                    scc.addToVariableList("show_back", false);
                    scc.activate("stuff");
                    break;
                }
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }catch(InventoryException e){
                System.out.println(e.getMessage());
            }
        }
    };

    @FXML
    private VBox button_container;
    
    @FXML
    private VBox stuff_container;

    @FXML
    private Text welcome_header;

    @FXML
    protected void showAll(ActionEvent event) throws SQLException, InventoryException{
        scc.addToVariableList("show_all", "true");
        scc.activate("category");
    }

    @FXML
    protected void createNewCategory(ActionEvent event) throws SQLException, InventoryException{
        TextInputDialog dialog = new TextInputDialog("Kategorian nimi tähän...");
        dialog.setTitle("Luo uusi kategoria");
        dialog.setHeaderText("Luo uusi kategoria kirjoittamalla sen nimi alle ja klikkaamalla \"OK\"");

        DialogPane pane = dialog.getDialogPane();
        pane.getStylesheets().add(getClass().getResource(Main.cssPath + Main.universal).toExternalForm());
        pane.getStyleClass().add("newCategoryDialog");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Category cat = new Category();
            if(cat.create(user.modelValues.get("id"), "TODO", result.get()).save().exists()){
                Button button = new Button(result.get());
                button.setId("category_" + cat.modelValues.get("id"));
                button.getStyleClass().add("row");
                button.setOnAction(button_handler);
                button_container.getChildren().add(button);
            }
        }        
    }

    @FXML
    protected void createNewStuff(ActionEvent event) throws SQLException, InventoryException{
        TextInputDialog dialog = new TextInputDialog("Tavaran nimi tähän...");
        dialog.setTitle("Luo uusi tavara");
        dialog.setHeaderText("Luo uusi tavara kirjoittamalla sen nimi alle ja klikkaamalla \"OK\"");

        DialogPane pane = dialog.getDialogPane();
        pane.getStylesheets().add(getClass().getResource(Main.cssPath + Main.universal).toExternalForm());
        pane.getStyleClass().add("newStuffDialog");

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Stuff stuff = new Stuff();            
            if(stuff.create(result.get(), "TODO", new Stuff().getNextOrder()).save().exists()){
                Button button = new Button(result.get());
                button.setId("stuff_" + stuff.modelValues.get("id"));
                button.getStyleClass().add("row");
                button.setOnAction(button_handler);                
                stuff_container.getChildren().add(button);
            }
        }
    }

    public void onLoad(HashMap<String, Object> variableList) throws SQLException, InventoryException{      
        this.user = scc.getUser(); 
        //next block sets personalized welcome header 
        String name = (String) this.user.modelValues.get("name");
        String welcome = welcome_header.getText();
        welcome = welcome.replace("x", name);
        welcome_header.setText(welcome);

        ArrayList<HashMap<String, Object>> categories = new Query().select("*")
                                                                   .from("category")
                                                                   .where("user_id", (int) user.modelValues.get("id"))
                                                                   .execute();

        button_container.getChildren().clear();
        for(HashMap<String, Object> row : categories){
            Button button = new Button(""+(String) row.get("name"));
            button.setId("category_" + row.get("id"));
            button.getStyleClass().add("row");
            button.setOnAction(button_handler);
            button_container.getChildren().add(button);
        }

        ArrayList<HashMap<String, Object>> stuff = new Query().select("*")
                                                              .from("stuff")
                                                              .execute();

        stuff_container.getChildren().clear();
        for(HashMap<String, Object> row : stuff){
            Button button = new Button((String) row.get("name"));
            button.setId("stuff_" +  row.get("id"));
            button.getStyleClass().add("row");
            button.setOnAction(button_handler);
            stuff_container.getChildren().add(button);
        }
    }

    @FXML
    protected void logOut(ActionEvent event)throws SQLException, InventoryException{
        scc.activate("login");
    }
}