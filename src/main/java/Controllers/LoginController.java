
package Controllers;

import java.util.HashMap;
import Models.User;
import SQLite.Query;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class LoginController extends Controller{
    public ScreenController scc;

    @FXML
    private TextField passwordField;

    @FXML
    private TextField username;

    @FXML
    protected void onEnter(ActionEvent event) throws Exception {
        tryLogin();
    }

    @FXML
    protected void submit(ActionEvent event) throws Exception {
        tryLogin();
    }

    private void tryLogin() throws Exception {
        User user = new User();
        boolean users_exists = true;
        try {
            HashMap<String, Object> results = new Query().select("*")
                                                        .from("users")
                                                        .where("password", passwordField.getText())
                                                        .where("username", username.getText())
                                                        .execute()
                                                        .get(0);
            int id = (int) results.get("id");
            user = (User) user.getById(id);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        } catch (IndexOutOfBoundsException e){
            users_exists = false;
        }
        if (user.exists() && users_exists) {
            scc.setUser(user);
            scc.activate("home");
        } else {
            passwordField.clear();
            System.out.println("Väärä salasana.");
        }
    }

    @FXML
    protected void createNewUser(ActionEvent event) throws Exception{        
        scc.activate("new");
    }

    public void onLoad(HashMap<String, Object> variableList){
           username.clear();
           passwordField.clear();
    }
}