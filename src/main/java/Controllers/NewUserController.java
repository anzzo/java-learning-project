
package Controllers;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;

import Exceptions.InventoryException;
import Models.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;

public class NewUserController extends Controller{
    
    public ScreenController scc;

    @FXML
    private TextField name;
    @FXML
    private TextField lastname;
    @FXML
    private TextField email;
    @FXML
    private TextField userName;
    @FXML
    private TextField passwordField;
    @FXML
    private TextField passwordField2;

    @FXML
    protected void createUser(ActionEvent event) throws SQLException, InventoryException{
        tryCreate();
    }

    @FXML
    protected void onEnter(ActionEvent event) throws SQLException, InventoryException {
        tryCreate();
    }

    @FXML
    protected void backToLogin(ActionEvent event) throws SQLException, InventoryException{
        scc.activate("login");
    }

    public boolean checkFields(){
        boolean ret = false;
        //checks that all fields are filled and that password fields match
        if( name.getLength() != 0 && lastname.getLength() != 0 && email.getLength() != 0 && userName.getLength() != 0 && passwordField.getLength() != 0 && passwordField2.getLength() != 0 && (passwordField
                .getText()).equals(passwordField2.getText())){
            ret = true;
        }
        return ret;
    }

    public void tryCreate() throws SQLException, InventoryException{
        if (checkFields()) {
            // Create new user
            User created = (User) new User().create(userName.getText(), name.getText(), lastname.getText(), email.getText(), passwordField.getText(), 1, 
                    new Timestamp(System.currentTimeMillis()), new Timestamp(System.currentTimeMillis())).save();

            // if created go to home
            if (created.exists()) {
                scc.setUser(created);
                scc.activate("home");
            } else {
                System.out.println("Virhe käyttäjän luonnissa.");
            }
        } else {
            passwordField.clear();
            passwordField2.clear();
            System.out.println("Salasana ei ollut sama.");
        }
    }

    public void onLoad(HashMap<String, Object> list){
        name.clear();
        lastname.clear();
        email.clear();
        userName.clear();
        passwordField.clear();
        passwordField2.clear();
    }

}