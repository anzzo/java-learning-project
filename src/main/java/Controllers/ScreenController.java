
package Controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import Exceptions.InventoryException;
import Models.User;
import javafx.scene.Parent;
import javafx.scene.Scene;

public class ScreenController extends Controller{

    private Scene scene;
    private User user;

    // To save views
    private HashMap<String, Parent> viewList           = new HashMap<>();
    private HashMap<String, Controller> controllerList = new HashMap<>();
    private HashMap<String, Object> variableList       = new HashMap<>();
    private List<String> viewOrder                     = new ArrayList<String>();

    /**
     * Constructor
     * @param scene {Scene} - set localvariable scene
     */
    public ScreenController(Scene scene) {
        this.scene = scene;
    }

    /**
     * Function which lets you add views. So that they can be easily changed.
     * @param name {String} - Name which we will use to change the view
     * @param parent {Parent} - root element of the view
     * @param controller {Controller} - views controller
     */    
    public void add(String name, Parent parent, Controller controller) {
        this.viewList.put(name, parent);
        this.controllerList.put(name, controller);
    }

    /**
     * Function that activas or changes views.
     * Calling this with a name that has been added changes view.
     * @param name {String} - Name of the view.
     * @throws SQLException
     * @throws InventoryException
     */
    public void activate(String name) throws SQLException, InventoryException {
        if(name.equals("back")){
            name = this.viewOrder.get(this.viewOrder.size() - 2);
        }
        scene.setRoot(viewList.get(name));
        this.onLoad(name);
        this.viewOrder.add(name);
    }

    /**
     * Replaces variablelist
     * @param map {HashMap<String, Object>} - List that will replace current list 
     */
    public void setVariableList(HashMap<String, Object> map){
        this.variableList = map;
    }

    /**
     * Clears variablelist
     */
    public void clearVariableList(){
        this.variableList.clear();
    }

    /**
     * Adds variable to list
     * @param name {String} - Key of the variable
     * @param value {Object} - Value of the variable
     */
    public void addToVariableList(String name, Object value){
        this.variableList.put(name, value);
    }

    /**
     * Gets user
     * @return {User} - User to be returned
     */
    public User getUser(){
        return this.user;
    }

    /**
     * Sets user.
     * @param user {User} - User to be setted
     */
    public void setUser(User user){
        this.user = user;
    }

    public void onLoad(String name) throws SQLException, InventoryException{
        switch(name){
            case "home": 
                HomeController hController = (HomeController) this.controllerList.get(name);
                hController.onLoad(variableList);
                break;
            case "category":
                CategoryController cController = (CategoryController) this.controllerList.get(name);
                cController.onLoad(variableList);
                break;
            case "stuff":
                StuffController sController = (StuffController) this.controllerList.get(name);
                sController.onLoad(variableList);
                break;
            case "login":
                LoginController lController = (LoginController) this.controllerList.get(name);
                lController.onLoad(variableList);
                break;
            case "new":
                NewUserController newController = (NewUserController) this.controllerList.get(name);
                newController.onLoad(variableList);
                break;
            default: break;
        }
        
    }
}