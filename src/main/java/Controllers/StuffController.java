
package Controllers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import Exceptions.InventoryException;
import Exceptions.ModelException;
import Models.CategoryRows;
import Models.Stuff;
import SQLite.Query;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

public class StuffController extends Controller{

    public ScreenController scc;
    public Stuff stuff;

    @FXML
    private Text header_text;

    @FXML
    private TextField stuff_name;

    @FXML
    private TextField stuff_type;

    @FXML
    private Button go_back_button;

    @FXML
    protected void toHomeView() throws Exception{
        scc.activate("home");
    }

    @FXML
    protected void goBack() throws Exception {
        scc.activate("back");
    }

    @FXML
    protected void saveStuff(ActionEvent event) throws SQLException, InventoryException{
        if(!this.stuff.exists()){
            throw new ModelException("Stuff that we tried to save doesn't exist");
        }
        this.stuff.set("name", stuff_name.getText())
                  .set("type", stuff_type.getText())
                  .save();
    }

    public void onLoad(HashMap<String, Object> variableList) throws SQLException, InventoryException{
        go_back_button.setVisible((boolean) variableList.get("show_back"));
        HashMap<String, Object> stuff = new Query().select("*")
                                                   .from(Stuff.TABLE)
                                                   .where("id", (int) variableList.get("stuff_id"))
                                                   .execute().get(0);
        this.stuff = new Stuff(stuff);
        if(!this.stuff.exists()){
            throw new ModelException("Stuff doesn't exists");
        }
                
        header_text.setText((String) stuff.get("name"));
        stuff_name.setText((String) stuff.get("name"));
        stuff_type.setText((String) stuff.get("type"));
    }

    @FXML
    protected void removeStuff(ActionEvent event) throws SQLException, InventoryException{
        int stuff_id = (int) this.stuff.modelValues.get("id");
        new Stuff().removeRow(stuff_id);

        ArrayList<HashMap<String, Object>> category_rows = new Query().select("*").from(CategoryRows.TABLE).where("stuff_id", stuff_id).execute();
        for(HashMap<String, Object> row : category_rows){
            new CategoryRows().removeRow((int) row.get("id"));
        }
        scc.activate("home");
    }
}