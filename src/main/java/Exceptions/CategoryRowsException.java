package Exceptions;

public class CategoryRowsException extends ModelException{
    /**
     *
     */
    private static final long serialVersionUID = 5028475211424737479L;

    public CategoryRowsException(String error) {
        super(error);
    }
}