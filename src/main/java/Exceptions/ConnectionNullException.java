package Exceptions;

public class ConnectionNullException extends InventoryException {
    /**
     *
     */
    private static final long serialVersionUID = -6188790089745658252L;

    public ConnectionNullException(String error) {
        super(error);
    }
}