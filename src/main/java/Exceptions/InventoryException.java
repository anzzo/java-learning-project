package Exceptions;

public class InventoryException extends Exception{
    /**
     *
     */
    private static final long serialVersionUID = -7601721523137241239L;

    public InventoryException(String error) {
        super(error);
    }
}