package Exceptions;

public class ModelException extends InventoryException{
    /**
     *
     */
    private static final long serialVersionUID = 2460822339447052490L;

    public ModelException(String error) {
        super(error);
    }
}