package Exceptions;

public class NodeNotFoundException extends InventoryException{
    /**
     *
     */
    private static final long serialVersionUID = -1115417355699259411L;

    public NodeNotFoundException(String error) {
        super(error);
    }
}