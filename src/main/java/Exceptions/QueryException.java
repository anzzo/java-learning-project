package Exceptions;

public class QueryException extends InventoryException {
    /**
     *
     */
    private static final long serialVersionUID = -2608097091120757110L;

    public QueryException(String error) {
        super(error);
    }
}