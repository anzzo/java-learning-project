package Exceptions;

public class UserGetException extends ModelException{
    /**
     *
     */
    private static final long serialVersionUID = 7320391275219830766L;

    public UserGetException(String error) {
        super(error);
    }
}