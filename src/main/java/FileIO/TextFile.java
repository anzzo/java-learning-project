
package FileIO;

import java.io.*;
import java.util.ArrayList;

public class TextFile{
    public ArrayList<String> fileText = new ArrayList<String>();
    public int lineCount = 0;
    public File f1 = null;
    public String path = "files" + File.separator + "text_files" + File.separator;
    public String absolutePath = null;
    public File here = null;

    public TextFile(){
        here = new File(".");
        absolutePath = here.getAbsolutePath();
        path = absolutePath + File.separator + path;
        File directory = new File(path);
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
    }

    public void readFile(String filename){
        try{            
            f1 = new File(path+filename);
            if(f1.exists()){
                FileReader fr1 = new FileReader(f1);
                BufferedReader in1 = new BufferedReader(fr1);
                String row = "";
                fileText.clear();
                while((row = in1.readLine()) != null){
                    fileText.add(row);
                    lineCount++;
                }
                in1.close();
                fr1.close();
            }else{
                f1.createNewFile();
            }
        }catch(Exception e){
            System.out.println("readFile Exception");
            System.out.println(e);
        }
    }

    public void writeFile(String text){
        this.fileText.add(text);
        try{
            if(f1.delete()){
                f1.createNewFile();
            }else{
                System.out.println(f1.getName() + " poisto epäonnistui");
            }            
            FileWriter fw1 = new FileWriter(f1);
            BufferedWriter bw1 = new BufferedWriter(fw1);
            int index = 0;
            for (String line : fileText) {
                bw1.write(line);
                if(index != (fileText.size() - 1)){
                    bw1.newLine();            
                }                
                index++;    
            }
            bw1.close();
            fw1.close();
        }catch(Exception e){
            System.out.println("writeFile Exception");
            System.out.println(e);
        }
    }

    public boolean deleteFile(){        
        boolean deleted = false;
        if(f1.exists()){
            if(f1.delete()){
                deleted = true;
            }            
        }
        return deleted;
    }
}