
package Main;

import java.io.File;
import java.sql.SQLException;
import java.util.Scanner;

import SQLite.Table;
import Controllers.CategoryController;
import Controllers.HomeController;
import Controllers.LoginController;
import Controllers.NewUserController;
import Controllers.ScreenController;
import Controllers.StuffController;
import Exceptions.ConnectionNullException;
import Models.Category;
import Models.CategoryRows;
import Models.Stuff;
import Models.User;
import FileIO.TextFile;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static final String xmlPath   = "/XML/";

    public static final String login     = "login.fxml";
    public static final String home      = "home.fxml";
    public static final String newUser   = "newUser.fxml";
    public static final String category  = "category.fxml";
    public static final String stuff     = "stuff.fxml";

    public static final String cssPath   = "/CSS/";
    public static final String universal = "universal.css";

    public static void main(String[] args){
        clearScreen();
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        //initializes database or creates new if not there
        initDB();
        
        // Loaders for every view
        FXMLLoader login_loader    = new FXMLLoader(getClass().getResource(Main.xmlPath + Main.login));
        FXMLLoader home_loader     = new FXMLLoader(getClass().getResource(Main.xmlPath + Main.home));
        FXMLLoader newUser_loader  = new FXMLLoader(getClass().getResource(Main.xmlPath + Main.newUser));
        FXMLLoader category_loader = new FXMLLoader(getClass().getResource(Main.xmlPath + Main.category));
        FXMLLoader stuff_loader    = new FXMLLoader(getClass().getResource(Main.xmlPath + Main.stuff));
        
        // roots for every view
        Parent login_parent    = (Parent) login_loader.load();
        Parent home_parent     = (Parent) home_loader.load();
        Parent newUser_parent  = (Parent) newUser_loader.load();
        Parent category_parent = (Parent) category_loader.load();
        Parent stuff_parent    = (Parent) stuff_loader.load();
        
        // scene which we use to change views
        Scene scene = new Scene(login_parent);
        stage.setMaximized(true);
        
        // Every views controllers
        LoginController   lController  = (LoginController) login_loader.getController();
        HomeController    hController  = (HomeController) home_loader.getController();
        NewUserController nuController = (NewUserController) newUser_loader.getController();
        CategoryController cController = (CategoryController) category_loader.getController();
        StuffController sController    = (StuffController) stuff_loader.getController();
        
        // Controller which handles changing views
        ScreenController scc = new ScreenController(scene);
        
        // add css from external file
        scene.getStylesheets().add(cssPath + universal);
        
        // initialize login screen
        stage.setTitle("Inventaario-työkalu");
        stage.setScene(scene);
        stage.show();
        
        // add views in screencontroller
        scc.add("login", login_parent, lController);
        scc.add("home", home_parent, hController);
        scc.add("new", newUser_parent, nuController);
        scc.add("category", category_parent, cController);
        scc.add("stuff", stuff_parent, sController);
        
        // add screencontroller in controllers for changing views
        lController.scc  = scc;
        nuController.scc = scc;
        hController.scc  = scc;
        cController.scc  = scc;
        sController.scc  = scc;        
    }
    
    public static void initDB() throws SQLException, ConnectionNullException{
        // Luodaan käyttäjä table, jos sitä ei ole jo
        Table users = new Table(User.TABLE);
        users.setColumn("id", "INTEGER", 1).setColumn("username", "text", 0).setColumn("name", "text", 0)
                .setColumn("lastname", "text", 0).setColumn("email", "text", 0).setColumn("password", "text", 0)
                .setColumn("access_level", "int", 0).setColumn("created_at", "datetime", 0)
                .setColumn("updated_at", "datetime", 0).execute();

        // Asetukset taulu
        // Table settings = new Table(Settings.TABLE);

        // category taulu
        Table cate = new Table(Category.TABLE);
        cate.setColumn("id", "INTEGER", 1).setColumn("user_id", "int", 0).setColumn("type", "int", 0).setColumn("name", "text", 0).execute();

        // category rows
        Table cat_rows = new Table(CategoryRows.TABLE);
        cat_rows.setColumn("id", "INTEGER", 1).setColumn("category_id", "int", 0).setColumn("stuff_id", "int", 0).setColumn("amount", "int", 0).setColumn("orderby", "int", 0).execute();

        // Luodaan stuff taulu
        Table stuff = new Table(Stuff.TABLE);
        stuff.setColumn("id", "INTEGER", 1).setColumn("name", "text", 0).setColumn("type", "text", 0).setColumn("orderby", "int", 0).execute();
    }

    public static void clearScreen() {
        try {
            if (System.getProperty("os.name").contains("Windows")) {
                new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (Exception ex) {
            System.out.println("clearScreen Exception");
            System.out.println(ex);
        }
    }

    public static void readTextFile(Scanner sc) {
        System.out.println("Anna tekstitiedoston nimi:");
        String name = sc.nextLine();
        TextFile file = new TextFile();
        file.readFile(name);
        System.out.println("=====FILE START=====");
        for (String line : file.fileText) {
            System.out.println("\t" + line);
        }
        System.out.println("=====FILE   END=====");
    }

    public static void writeTextFile(Scanner sc) {
        System.out.println("Anna kirjoitettavan tiedoston nimi:");
        String name = sc.nextLine();
        TextFile file = new TextFile();
        file.readFile(name);
        System.out.println("Kuinka monta riviä tahdot kirjoittaa:");
        int rows = sc.nextInt();
        sc.nextLine();
        System.out.println("Anna kirjoitettava teksti:");
        String write = "";
        for (int i = 0; i < rows; i++) {
            write = sc.nextLine();
            file.writeFile(write);
        }
    }

    public static void removeTextFile(Scanner sc) {
        System.out.println("Anna poistettavan tiedoston nimi:");
        TextFile file = new TextFile();
        String name = sc.nextLine();
        file.readFile(name);
        boolean deleted = file.deleteFile();
        if (deleted) {
            System.out.println("Tiedosto " + file.f1.getName() + " poistettu.");
        } else {
            System.out.println("Tiedostoa " + file.f1.getName() + " EI poistettu.");
        }
    }

    public static File[] getFilenames(String path) {
        File databases = new File(path);
        File[] files = databases.listFiles();
        if (files == null) {
            System.out.println("Ei löytynyt yhtään tiedostoa.");
        }
        return files;
    }

    public static void printFilenames(String path) {
        File[] files = getFilenames(path);
        if (files != null) {
            for (File file : files) {
                System.out.println(file.getName());
            }
        }
    }
}