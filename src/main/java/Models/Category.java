
package Models;

import java.sql.SQLException;
import java.util.HashMap;

import Exceptions.InventoryException;

public class Category extends Model{
    /**
     * Tablename that is linked to this Model
     */
    public static final String TABLE = "category";

    /**
     * Constructor that passes tablename to super class.
     * @throws SQLException
     * @throws InventoryException
     */
    public Category() throws SQLException, InventoryException {
        super(TABLE);        
    }

    /**
     * Overloaded constructor
     * Constructor that passes tablename and wanted values to superclass
     * @param model {HashMap<String, Object>} - Return value of a Query-object.
     * @throws SQLException
     * @throws InventoryException
     */
    public Category(HashMap<String, Object> model) throws SQLException, InventoryException {
        super(TABLE, model);
    }
}