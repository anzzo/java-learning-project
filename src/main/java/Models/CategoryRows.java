
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import Exceptions.InventoryException;
import SQLite.Database;

public class CategoryRows extends Model {
    /**
     * Tablename that is linked to this Model
     */
    public static final String TABLE = "categoryRows";
    
    /**
     * Constructor that passes tablename to superclass.
     * @throws SQLException
     * @throws InventoryException
     */
    public CategoryRows() throws SQLException, InventoryException{
        super(TABLE);
    }

    /**
     * Overloaded constructor 
     * Constructor that passes tablename and wanted values to superclass
     * @param model {HashMap<String, Object>} - Return value of a Query-object.
     * @throws SQLException
     * @throws InventoryException
     */
    public CategoryRows(HashMap<String, Object> model) throws SQLException, InventoryException {
        super(TABLE, model);
    }

    /**
     * Adds one to amount column in chosen row.
     * @param category_id {int} - chosen row
     * @throws SQLException
     */
    public static void addOne(int category_id) throws SQLException{
        String add = "UPDATE " + TABLE + " SET amount = amount + 1 WHERE id = ?";
        Database db = new Database();
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); PreparedStatement pstmt = conn.prepareStatement(add)){
            pstmt.setInt(1, category_id);        
            pstmt.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }        
    }

    /**
     * Removes one from amount column in chosen row.
     * @param category_id {int} - chosen row
     * @throws SQLException
     */
    public static void decreaseOne(int category_id) throws SQLException {
        String add = "UPDATE " + TABLE + " SET amount = amount - 1 WHERE id = ?";
        Database db = new Database();
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); PreparedStatement pstmt = conn.prepareStatement(add)){
            pstmt.setInt(1, category_id);
            pstmt.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }
}