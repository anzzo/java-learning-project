
package Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;

import Exceptions.InventoryException;
import Exceptions.ModelException;
import SQLite.Database;
import SQLite.Query;

abstract public class Model{

    public HashMap<String, Object> modelValues = new HashMap<String, Object>();
    public String[] columnNames;
    public String TABLE = null;
    
    /**
     * Class constructor. Fills variables so we can use methods.
     * @param TABLE {String} - name of the database table
     * @throws SQLException
     * @throws InventoryException
     */
    public Model(String TABLE) throws SQLException, InventoryException{
        this.TABLE = TABLE;
        this.columnNames = Query.getColumnNames(TABLE);
        for (String columnName : this.columnNames) {
            if(columnName.equals("id")){
                this.modelValues.put(columnName, 0); continue;
            }
            this.modelValues.put(columnName, "");
        }
    }
    
    /**
     * Overloaded class constuctor. Fills all fields of the model
     * @param TABLE {String} - name of the database table
     * @param model {HashMap<String, Object>} - row from database
     * @throws SQLException
     * @throws InventoryException
     */
    public Model(String TABLE, HashMap<String, Object> model) throws SQLException, InventoryException {
        this.TABLE = TABLE;
        this.columnNames = Query.getColumnNames(TABLE);
        for (String columnName : this.columnNames) {
            this.modelValues.put(columnName, model.get(columnName));
        }
    }
 
    /**
     * Saves model to database. Creates new if id is not set and if id is set updates it.
     * @return {Model} - Returns this model
     * @throws SQLException
     * @throws InventoryException
     */
    public Model save() throws SQLException, InventoryException{    
        String create = this.getCreateSQL();
        if(create == ""){
            throw new ModelException("Create statement can't be empty.");
        }
        String update = this.getUpdateSQL();
        if(update == ""){
            throw new ModelException("Update statement can't be empty.");
        }
        Database db = new Database();
        int i = 1;
        if ((int) this.modelValues.get("id") == 0) {
            this.modelValues.put("id", this.getNextId());
            try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); PreparedStatement pstmt = conn.prepareStatement(create)){
                for(String name : this.columnNames){
                    if (name.equals("id")) {
                        continue;
                    }
                    if(name.equals("orderby")){
                        pstmt.setObject(i, this.getNextOrder());
                    }else {
                        pstmt.setObject(i, this.modelValues.get(name));
                    }
                    i++;
                }                
                pstmt.executeUpdate();
            }catch(SQLException e){
                System.out.println(e.getMessage());
            }
        } else {
            try (Connection conn = DriverManager.getConnection(db.getConnectionURL()); PreparedStatement pstmt = conn.prepareStatement(update)) {
                for (String name : this.columnNames) {
                    if (name.equals("id")) {
                        pstmt.setObject(this.columnNames.length, this.modelValues.get(name));
                    }else{
                        pstmt.setObject(i, this.modelValues.get(name));
                        i++;
                    }
                }                
                pstmt.executeUpdate();
            } catch (SQLException e) {
                System.out.println(e.getMessage());
            }
        }
        return this;
    }

    /**
     * Returns create-type SQL-string that matches given table.
     * @return {String} - SQL create statement
     */
    private String getCreateSQL(){
        String create = "";
        create += "INSERT INTO ";
        create += TABLE;
        create += " (";
        int i = 0;
        for (String name : this.columnNames) {
            if (i == 0) {
                i++;
                continue;
            }
            create += name;
            if (i < (this.columnNames.length - 1)) {
                create += ", ";
            }
            i++;
        }
        create += ") VALUES (";
        for (i = 0; i < this.columnNames.length; i++){
            if (i == 0) {
                continue;
            }
            create += "?";
            if (i < (this.columnNames.length - 1)) {
                create += ", ";
            }
        }
        create += ");";
        return create;
    }

    /**
     * Returns update-type SQL-string that matches table.
     * @return {String} - SQL update statement
     */
    private String getUpdateSQL(){
        String update = "";
        update += "UPDATE ";
        update += TABLE;
        update += " SET ";
        int i = 0;
        for (String name : columnNames) {
            if (i == 0) {
                i++;
                continue;
            }
            update += name;
            update += " = ?";
            if (i < (columnNames.length - 1)) {
                update += ", ";
            }
            i++;
        }
        update += " WHERE ";
        update += "id = ?;";
        return update;
    }

    /**
     * Gets next avaiable id from database.
     * @return {int} - Returns next avaiable int that will be assigned to row when saved.
     * @throws SQLException
     * @throws ModelException
     */
    public int getNextId() throws SQLException, InventoryException{
        if(this.TABLE == null){
            throw new ModelException("Table name is not initialized");
        }
        return (int) new Query().select("IFNULL(max(id), 0) + 1 AS id").from(this.TABLE).execute().get(0).get("id");
    }

    /**
     * Gets next avaiable orderby-value from database.
     * @return {int} - next avaiable orderby-value from database 
     * @throws SQLException
     * @throws ModelException
     */
    public int getNextOrder() throws SQLException, InventoryException {
        if (this.TABLE == null) {
            throw new ModelException("Table name is not initialized");
        }
        return (int) new Query().select("IFNULL(max(orderby), 0) + 1 AS orderby").from(this.TABLE).execute().get(0).get("orderby");
    }

    /**
     * Gets model from database and sets values. 
     * @param id - The id we use to search
     * @return {Model} - Return model that we searched
     * @throws SQLException
     * @throws ModelException
     */
    public Model getById(int id) throws SQLException, InventoryException {
        if (id == 0) {
            throw new ModelException("Cannot search with id 0");
        }
        if(this.TABLE == null){
            throw new ModelException("Table name is not initialized");
        }
        HashMap<String, Object> first = new Query().select("*").from(this.TABLE).where("id", id).execute().get(0);
        for (String column : this.columnNames) {
            this.modelValues.put(column, first.get(column));
        }
        return this;
    }

    /**
     * Fills model values and is ready to be saved after this.
     * Doesn't fill the "id" field. 
     * @param arr {String, int, Date} - input values
     * @return {Model} - returns model
     * @throws ModelException
     */
    public Model create(Object... arr) throws ModelException {        
        if (arr.length != (this.columnNames.length - 1)) {
            throw new ModelException("Not enough variables given to create-function");
        }
        int i = 0;
        for (String column : this.columnNames) {
            if(column.equals("id")){
                continue;
            }
            this.modelValues.put(column, arr[i]);
            i++;
        }
        return this;
    }

    /**
     * Checks if the id field is set. Meaning that model exists
     * @return {boolean} - true if model exists and false if not
     */
    public boolean exists(){
        if((int) this.modelValues.get("id") != 0){
            return true;
        }
        return false;
    }

    /**
     * Removes one row with given id.
     * @param model_id {int} - given row id
     * @throws SQLException
     */
    public void removeRow(int model_id) throws SQLException {
        String add = "DELETE FROM " + this.TABLE + " WHERE id = ?";
        Database db = new Database();
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); PreparedStatement pstmt = conn.prepareStatement(add)){
            pstmt.setInt(1, model_id);
            pstmt.executeUpdate();
        }catch(SQLException e){
            System.out.println(e.getMessage());
        }
    }

    /**
     * Sets one value to a new one. Must be saved to make permanent changes.
     * @param column {String} - Name of the column we want to assign new value
     * @param value {Object} - New value to be assigned
     * @return {Model} - returns itself to enable chaining.
     * @throws InventoryException
     */
    public Model set(String column, Object value) throws InventoryException{
        if(!this.modelValues.containsKey(column)){
            throw new ModelException("Tried to assign value with wrong key");
        }
        if((int) this.modelValues.get("id") == 0 && column.equals("id")){
            throw new ModelException("Can't assign new id");
        }
        this.modelValues.put(column, value);
        return this;
    }    
}