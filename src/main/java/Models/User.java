
package Models;

import java.sql.SQLException;
import java.util.HashMap;

import Exceptions.InventoryException;

public class User extends Model{
    /**
     * Tablename that is linked to this Model
     */
    
    public static final String TABLE = "users";
    /**
     * Constructor that passes tablename to superclass
     * @throws SQLException
     * @throws InventoryException
     */
    public User() throws SQLException, InventoryException{
        super(TABLE);           
    }

    /**
     * Overloaded constructor 
     * Constructor that passes tablename and wanted values to superclass
     * @param model {HashMap<String, Object>} - Return value of a Query-object.
     * @throws SQLException
     * @throws InventoryException
     */
    public User(HashMap<String, Object> model) throws SQLException, InventoryException {
        super(TABLE, model);
    }
}