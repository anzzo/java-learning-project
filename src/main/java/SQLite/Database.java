
package SQLite;

import java.io.File;

public class Database {            
    private String name     = null;
    private String url      = "";

    /**
     * Constructor that connects to named database or if not found creates one.
     */
    public Database() {
        name = "app_database";
        File here = new File(".");
        String absolutePath = here.getAbsolutePath();
        String path = absolutePath + File.separator + "files" + File.separator + "databases" + File.separator;
        File directory = new File(path);
        if (!directory.isDirectory()) {
            directory.mkdirs();
        }
        if (!name.contains(".db")) {
            name += ".db";
        }
        url = "jdbc:sqlite:" + path + name;
    }

    public String getConnectionURL(){
        return this.url;
    }

    /**
     * Returns name of the database
     * @return {String} - name of the database
     */
    public String getName() {        
        return name;
    }  
}