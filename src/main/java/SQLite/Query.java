
package SQLite;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import Exceptions.ConnectionNullException;
import Exceptions.InventoryException;
import Exceptions.QueryException;

public class Query {

    private String sql = "";

    private boolean whereFlag = false;

    private boolean debug = false;

    //For validating query
    private boolean select   = false;
    private boolean from     = false;
    private boolean executed = false;

    /**
     * Adds select statement to query 
     * @param columns {String} - Database columns to be selected
     * @return {Query} - for chaining methods
     */
    public Query select(String... columns) throws InventoryException{
        if(this.executed){
            throw new QueryException("Query is already executed.");
        }
        int i = 0;
        sql = "";
        sql += "SELECT";
        sql += " ";
        for (String column : columns) {
            sql += column;
            if (i < (columns.length - 1)) {
                sql += ",";
            }
            sql += " ";
            i++;
        }        
        sql += "FROM";
        sql += " ";
        this.select = true;
        return this;
    }

    /**
     * Adds from {String} to query
     * @param table {String} - table name
     * @return {Query} - for chaining methods
     */
    public Query from(String table) throws QueryException{
        if (this.executed) {
            throw new QueryException("Query is already executed.");
        }
        if(sql.equals("")){
            throw new QueryException("SQL can't be empty if from-function is called");
        }
        if(!this.select){
            throw new QueryException("select-function must be called before from-function");
        }
        sql += "\"";
        sql += table;
        sql += "\"";
        sql += " ";
        this.from = true;
        return this;
    }

    /**
     * Adds where statement to query
     * @param column {String} - column name
     * @param value {String} - to which we compare column value
     * @return {Query} - for chaining methods
     */
    public Query where(String column, String value) throws QueryException{
        if (this.executed) {
            throw new QueryException("Query is already executed.");
        }
        if(!this.select || !this.from){
            throw new QueryException("where-function called too soon");
        }
        if(this.whereFlag){
            this.and(column, value);
            return this;
        }
        this.whereFlag = true;        
        sql += "\n";
        sql += "WHERE";
        sql += " ";
        sql += "\"";
        sql += column;
        sql += "\"";
        sql += " ";
        sql += "=";
        sql += " ";
        sql += "\"";
        sql += value;
        sql += "\"";
        sql += " ";
        return this;
    }

    /**
     * Adds where statemetn to query. Overloaded where statement so we can search with {int} values
     * @param column {String} - column name
     * @param value {int} - to which we compare column value 
     * @return {Query} - for chaining methods
     */
    public Query where(String column, int value) throws QueryException{
        if (this.executed) {
            throw new QueryException("Query is already executed.");
        }
        if(!this.select || !this.from){
            throw new QueryException("where-function called too soon");
        }
        if(this.whereFlag){
            this.and(column, value);
            return this;
        }
        this.where(column, ""+value);
        return this;
    }

    /**
     * If where is already added use this to add and-statement to query
     * @param column {String} - column name
     * @param value  {String} - to which we compare column value
     * @return {Query} - for chaining methods
     */
    private Query and(String column, String value) {
        sql += "\n";
        sql += "AND";
        sql += " ";
        sql += "\"";
        sql += column;
        sql += "\"";
        sql += " ";
        sql += "=";
        sql += " ";
        sql += "\"";
        sql += value;
        sql += "\"";
        sql += " ";
        return this;
    }

    /**
     * Adds and-statement to query. Overload for {int} values     
     * @param column {String} - column name
     * @param value {int} - to which we comapre column value
     * @return {Query} - for chaining methods
     */
    private Query and(String column, int value){
        this.and(column, ""+value);
        return this;
    }

    /**
     * Runs the query and packs ResultSet into arraylist hashmap 
     * @return ArrayList<HashMap<String, Object>>
     * @throws Exception
     */
    public ArrayList<HashMap<String, Object>> execute() throws SQLException, ConnectionNullException, QueryException {
        if (this.executed) {
            throw new QueryException("Query is already executed.");
        }
        if(sql.equals("")){
            throw new QueryException("SQL that we are trying to run is empty.");
        }
        if(!this.select || !this.from){
            throw new QueryException("Tried to call execute-function too soon.");
        }
        ArrayList<HashMap<String, Object>> table = new ArrayList<HashMap<String, Object>>();
        sql += ";";
        if(debug){
            System.out.println("==========RUNNEDSQLSTART==========");
            System.out.println(sql);
            System.out.println("==========RUNNEDSQLEND==========");
        }

        Database db = new Database();
        System.out.println("=====QUERYEXECUTECONNECTIONSTARTED=====");
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); Statement stmt = conn.createStatement(); ResultSet resultSet = stmt.executeQuery(sql)){
            ResultSetMetaData meta = resultSet.getMetaData();
            while(resultSet.next()){
                HashMap<String, Object> row = new HashMap<String, Object>();
                for(int i = 1; i <= meta.getColumnCount(); i++){
                    row.put(meta.getColumnName(i), resultSet.getObject(i));
                }
                table.add(row);
            }
        }catch(SQLException e){
            System.out.println("QUERYEXECUTE EXCEPTION");
        }
        System.out.println("=====QUERYEXECUTECONNECTIONENDED=====");
        return table;
    }

    /**
     * Takes in table name and returns String[] of column names.
     * @param tableName {String} - table name
     * @return {String[]} - tables column names
     */
    public static String[] getColumnNames(String tableName) throws SQLException{
        String[] columnNames = new String[0];
        Database db = new Database();  
        System.out.println("=====GETCOLUMNNAMESCONNECTIONSTARTED=====");      
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); Statement stmt = conn.createStatement(); ResultSet resultSet = stmt.executeQuery("SELECT * FROM " + tableName + " LIMIT 1")){
            ResultSetMetaData meta = resultSet.getMetaData();
            columnNames = new String[meta.getColumnCount()];
            for(int i = 1; i <= columnNames.length; i++){
                columnNames[i - 1] = meta.getColumnName(i);
            }
        }catch(SQLException e){
            System.out.println("GETCOLUMNNAMES EXCEPTION");
        }
        System.out.println("=====GETCOLUMNNAMESCONNECTIONENDED=====");
        return columnNames;
    }

    /**
     * Orders results by given column ascending or descending
     * Must be called before execute and after possible wheres.
     * @param column {String} - column name which we use to order results
     * @param ascdesc {String} - determines either ascending or descending 
     * @return {Query} . fro chaining methods
     * @throws QueryException
     */
    public Query orderBy(String column, String ascdesc) throws QueryException{
        if (this.executed) {
            throw new QueryException("Query is already executed.");
        }
        if(column == null || !ascdesc.equals("ASC") || !ascdesc.equals("DESC")){
            throw new QueryException("Arguments aren't valid");
        }
        if(!this.select || !this.from){
            throw new QueryException("orderBy-function called in wrong order.");
        }
        return this;
    } 
}