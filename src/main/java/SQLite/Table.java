package SQLite;

import java.sql.*;
import java.util.ArrayList;

import Exceptions.ConnectionNullException;

public class Table {
    private String name                   = null;
    private String sql                    = "";
    public  ArrayList<String> columnNames = new ArrayList<String>();

    /**
     * Constructor that gets connection to given database and calls setName()-function
     * @param databaseName {String} - Name of the connected database
     * @param tableName {String} - Name of the table to be created
     */
    public Table(String tableName) {
        setName(tableName);
    }

    /**
     * Sets name to query that creates table
     * @param tableName {String} - Tables name
     * @return {Table} - for chaining methods
     */
    private Table setName(String tableName) {
        sql += "CREATE TABLE IF NOT EXISTS " + tableName + " (\n";
        return this;
    }

    /**
     * Adds column to table. Calls addKey()-function. 
     * @param columnName {String} - name of the column to be created
     * @param type       {String} - type of column to be created
     * @param key        {int} - which key we should give to column
     * @return {Table} - for chaining methods
     */
    public Table setColumn(String columnName, String type, int key) {
        sql += columnName + " " + type;
        addKey(key);
        sql += ",\n";
        columnNames.add(columnName);
        return this;
    }

    /**
     * Creates new column to a existing table. Calls addKey()-function.
     * @param columnName {String} - name of the column
     * @param type {String} - type of the column
     * @param key {int} - which key we should give to column
     * @return {Table} - for chaining methods
     */
    public Table newColumn(String columnName, String type, int key) {
        sql = "";
        sql += "ALTER TABLE ";
        sql += name;
        sql += " ADD ";
        sql += columnName;
        sql += " " + type;
        addKey(key);
        columnNames.add(columnName);
        return this;
    }

    /**
     * Adds key to column
     * @param key {int} - used in switch case to add wanted key.
     */
    private void addKey(int key) {
        switch (key) {
            case 1:
                sql += " PRIMARY KEY";
                break;
            default:
                break;
        }
    }

    /**
     * Executes the query that has been build.
     * @return {Table}
     */
    public Table execute() throws SQLException, ConnectionNullException{
        if (sql.charAt(sql.length() - 2) == ',') {
            sql = sql.substring(0, sql.length() - 2);
            sql += "\n";
        }
        sql += ");";
        Database db = new Database();
        System.out.println("=====TABLEEXECUTECONNECTIONSTARTED=====");
        try(Connection conn = DriverManager.getConnection(db.getConnectionURL()); Statement stmt = conn.createStatement()){
            stmt.execute(sql);
        }catch(SQLException e){
            System.out.println("TABLEEXECUTE EXCEPTION");
        }
        System.out.println("=====TABLEEXECUTECONNECTIONENDED=====");
        return this;
    }
}
